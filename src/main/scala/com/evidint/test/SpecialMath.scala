package com.evidint.test

/**
 * A translation of the specialMath Python script with an alternative
 * implementation
 *
 * @author Jason Crocker
 */
class SpecialMath {
  def calculate0(x: Int): Int = x match {
    case 0 => 0
    case 1 => 1
    case _ => x + calculate0(x - 1) + calculate0(x - 2)
  }

  def calculate1(n: Int): Long = {
    var prev = 0L
    var curr = 0L
    var next = 0L
    for(count <- 1 to n) {
      next = count + prev + curr
      prev = curr
      curr = next
    }
    curr

  }
}
