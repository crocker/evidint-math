package com.evidint.test

import org.specs2.mutable.Specification

class SpecialMathTest extends Specification {

  "Special math" should {
    val sm = new SpecialMath

    "produce 79 when given an input of 7 " in {
      val input = 7
      val output = 79
      sm.calculate0(input) mustEqual output
      sm.calculate1(input) mustEqual output
    }

    "produce 10926 when given an input of 17 " in {
      val input = 17
      val output = 10926
      sm.calculate0(input) mustEqual output
      sm.calculate1(input) mustEqual output
    }

    "produce 1293530146158671458 when given an input of 90 " in {
      val input = 90
      val output = 1293530146158671458L
      //sm.calculate0(input) mustEqual output
      sm.calculate1(input) mustEqual output
    }
  }

}